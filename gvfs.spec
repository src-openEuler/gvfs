%global avahi_version 0.6
%global fuse_version 3.0.0
%global glib2_version 2.70.0
%global gsettings_desktop_schemas_version 3.33.0
%global goa_version 3.17.1
%global gudev_version 147
%global libarchive_version 3.0.22
%global libcdio_paranoia_version 0.78.2
%global libgcrypt_version 1.2.2
%global libgdata_version 0.18.0
%global libgphoto2_version 2.5.0
%global libimobiledevice_version 1.2
%global libmtp_version 1.1.15
%global libnfs_version 1.9.8
%global libplist_version 2.2
%global libsmbclient_version 4.12.0
%global libsoup_version 3.0.0
%global libusb_version 1.0.21
%global systemd_version 206
%global talloc_version 1.3.0
%global udisks2_version 1.97

Name:          gvfs
Version:       1.54.4
Release:       1
Summary:       gvfs is a backends for the gio framework in GLib
License:       LGPL-2.0-or-later AND GPL-3.0-only AND MPL-2.0 AND BSD-3-Clause-Sun
URL:           https://wiki.gnome.org/Projects/gvfs

Source0:       https://download.gnome.org/sources/gvfs/1.54/gvfs-%{version}.tar.xz



BuildRequires: /usr/bin/ssh meson gcc libexif-devel gettext-devel docbook-style-xsl chrpath
BuildRequires: pkgconfig(dbus-1)
BuildRequires: pkgconfig(gcr-4)
BuildRequires: pkgconfig(libxslt)
BuildRequires: pkgconfig(libsecret-1)
BuildRequires: pkgconfig(libbluray)
BuildRequires: pkgconfig(libcap)
BuildRequires: pkgconfig(polkit-gobject-1)
BuildRequires: pkgconfig(glib-2.0) >= %{glib2_version}
BuildRequires: pkgconfig(gsettings-desktop-schemas) >= %{gsettings_desktop_schemas_version}
BuildRequires: pkgconfig(libcdio_paranoia) >= %{libcdio_paranoia_version}
BuildRequires: pkgconfig(gudev-1.0) >= %{gudev_version}
BuildRequires: pkgconfig(libsoup-3.0) >= %{libsoup_version}
BuildRequires: pkgconfig(avahi-client) >= %{avahi_version}
BuildRequires: pkgconfig(avahi-glib) >= %{avahi_version}
BuildRequires: pkgconfig(udisks2) >= %{udisks2_version}
BuildRequires: systemd-devel >= %{systemd_version}
BuildRequires: pkgconfig(fuse3) >= %{fuse_version}
BuildRequires: libsmbclient-devel >= %{libsmbclient_version}
BuildRequires: pkgconfig(talloc) >= %{talloc_version}
BuildRequires: pkgconfig(libgphoto2) >= %{libgphoto2_version}
BuildRequires: pkgconfig(libimobiledevice-1.0) >= %{libimobiledevice_version}
BuildRequires: pkgconfig(libplist-2.0) >= %{libplist_version}
BuildRequires: libgcrypt-devel >= %{libgcrypt_version}
BuildRequires: pkgconfig(libmtp) >= %{libmtp_version}
BuildRequires: pkgconfig(libusb-1.0) >= %{libusb_version}
BuildRequires: pkgconfig(libnfs) >= %{libnfs_version}
BuildRequires: pkgconfig(goa-1.0) >= %{goa_version}
BuildRequires: pkgconfig(libgdata) >= %{libgdata_version}
BuildRequires: pkgconfig(libarchive) >= %{libarchive_version}

Requires:      glib2%{?_isa} >= %{glib2_version} udisks2 >= %{udisks2_version}
Requires:      fuse3 >= %{fuse_version} gsettings-desktop-schemas >= %{gsettings_desktop_schemas_version}
Requires:      libgdata%{?_isa} >= %{libgdata_version} usbmuxd
Requires:      %{name}-client%{?_isa} = %{version}-%{release}
Requires:      polkit

Provides:      %{name}-fuse3 %{name}-smb %{name}-archive %{name}-gphoto2 %{name}-afc %{name}-afp %{name}-mtp %{name}-goa %{name}-tests %{name}-nfs
Obsoletes:     %{name}-fuse3 %{name}-smb %{name}-archive %{name}-gphoto2 %{name}-afc %{name}-afp %{name}-mtp %{name}-goa %{name}-tests %{name}-nfs
Obsoletes:     gnome-mount <= 0.8 gnome-mount-nautilus-properties <= 0.8
Obsoletes:     gvfs-obexftp < 1.17.91-2
Obsoletes:     %{name} < 1.9.4-1

%description
Gvfs is a userspace virtual filesystem implementation for GIO (a library available in GLib).
It comes with a set of backends, including trash support, SFTP, SMB, HTTP, DAV, and many others.
Gvfs also contains modules for GIO that implement volume monitors and persistent metadata storage.

%package  client
Summary:  Client modules of backends for the gio framework in GLib
Obsoletes: python2-samba
Conflicts: %{name} < 1.25.2-2

%description client
This package containers the client modules of backend implementations for the gio framework in GLib.

%package devel
Summary: Development files for gvfs
Requires: %{name}-client%{?_isa} = %{version}-%{release}
Requires: %{name}%{?_isa} = %{version}-%{release}

%description devel
This package containers the headers and other files which are required for develop applications with gvfs.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
%meson -Dinstalled_tests=true \
       -Dman=true \
       -Dnfs=false \
       -Dbluray=false \
       -Dafc=false \
       -Darchive=false \
       -Dafp=false \
       -Dgcrypt=false \
       -Donedrive=false \
       -Dgoogle=false \
        %{nil}
%meson_build

%install
%meson_install
chrpath -d %{buildroot}%{_libdir}/gio/modules/*.so
chrpath -d %{buildroot}%{_libdir}/gvfs/*.so
chrpath -d %{buildroot}%{_libexecdir}/gvfs*
mkdir -p %{buildroot}/etc/ld.so.conf.d
echo "%{_libdir}/gio/modules" > %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf
echo "%{_libdir}/gvfs" >> %{buildroot}/etc/ld.so.conf.d/%{name}-%{_arch}.conf

# trashlib is GPLv3, include the license
cp -p daemon/trashlib/COPYING COPYING.GPL3

%find_lang gvfs

%post
# Reload .mount files:
killall -USR1 gvfsd >&/dev/null || :

%files
%{_datadir}/dbus-1/services/org.gtk.vfs.*.service
%{_datadir}/gvfs/mounts/*.mount
%{_datadir}/gvfs/remote-volume-monitors/*.monitor
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/polkit-1/actions/org.gtk.vfs.file-operations.policy
%{_datadir}/polkit-1/rules.d/org.gtk.vfs.file-operations.rules
%{_libdir}/gvfs/libgvfsdaemon.so
%{_libexecdir}/gvfs-*
%{_libexecdir}/gvfsd
%{_libexecdir}/gvfsd-*
%{_userunitdir}/gvfs-*.service
%{_tmpfilesdir}/gvfsd-fuse-tmpfiles.conf

%files client -f gvfs.lang
%license COPYING COPYING.GPL3
%doc NEWS README.md
%{_libdir}/gvfs/libgvfscommon.so
%{_libdir}/gio/modules/*.so
%config(noreplace) /etc/ld.so.conf.d/*

%files devel
%{_includedir}/gvfs-client/gvfs/gvfs*.h
%{_libexecdir}/installed-tests/gvfs
%{_datadir}/installed-tests

%files help
%{_mandir}/man1/gvfsd.1*
%{_mandir}/man1/gvfsd-metadata.1*
%{_mandir}/man7/gvfs.7*
%{_mandir}/man1/gvfsd-fuse.1*

%changelog
* Sat Oct 19 2024 Funda Wang <fundawang@yeah.net> - 1.54.4-1
- update to 1.54.4

* Wed Jul 17 2024 zhangxingrong-<zhangxingrong@uniontech.cn> - 1.53.91-3
- ftp: Fix data connection to IPv6 link-local address
- onedrive: Fix memory leaks

* Wed Apr 17 2024 liweigang <liweiganga@uniontech.com> - 1.53.91-2
- fix warning file list twice

* Mon Mar 11 2024 liweigang <liweiganga@uniontech.com> - 1.53.91-1
- update to version 1.53.91

* Fri Jan 12 2024 wangxiaomeng <wangxiaomeng@kylinos.cn> - 1.50.2-6
- Modify memory-leaks patch errors

* Thu Jul 20 2023 houlifei <houlifei@uniontech.com> - 1.50.2-5
- Fix memory leaks detected by valgrind

* Wed Mar 22 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.2-4
- add library path file to /etc/ld.so.conf.d

* Mon Mar 13 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.2-3
- delete  invalid line

* Mon Mar 13 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.2-2
- remove rpath

* Mon Jun 6 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1.50.2-1
- Update to 1.50.2

* Wed Jul 21 2021 yushaogui <yushaogui@huawei.com> - 1.46.2-2
- Delete a buildrequires for gdb 

* Mon May 31 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 1.46.2-1
- Upgrade to 1.46.2
- Update Version, Release, Source0, BuildRequires, Requires, Obsoletes
- Delete patch that existed in new version.
- Update stage 'build', 'files', global variable quantity

* Tue Oct 13 2020 zhanzhimin <zhanzhimin@huawei.com> - 1.40.2-8
- add gvfs-fuse(x86-64) dependency

* Wed Aug 12 2020 chengguipeng<chengguipeng1@huawei.com> - 1.40.2-7
- Resolve the compilation failure caused by the libplist upgrade.

* Sat Mar 21 2020 songnannan <songnannan2@huawei.com> - 1.40.2-6
- bugfix about update

* Fri Mar 20 2020 songnannan <songnannan2@huawei.com> - 1.40.2-5
- add gdb in buildrequires

* Tue Jan 14 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.40.2-4
- Add post script

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.40.2-3
- Delete redundant info

* Mon Jan 13 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.40.2-2
- Delete unneeded build requires

* Tue Jan 7 2020 openEuler Buildteam <buildteam@openeuler.org> - 1.40.2-1
- update to 1.40.2

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.38.1-3
- Type:cves
- ID:CVE-2019-12447 CVE-2019-12448 CVE-2019-12449
- SUG:restart
- DESC:fix CVE-2019-12447 CVE-2019-12448 CVE-2019-12449

* Thu Sep 26 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.38.1-2
- Package init
